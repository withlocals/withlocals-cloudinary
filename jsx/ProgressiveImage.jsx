import React from 'react';
import LazyImageWrapper from './LazyImage';
import Cloudinary from './cloudinary_utils';
import PropTypes from 'prop-types'

const SMALLSIZE = 20;
const QUALITY = 30;
const BLUR = 40;

class ProgressiveImage extends React.PureComponent {
  static propTypes = {
    src:PropTypes.string.isRequired,
    height:PropTypes.number.isRequired,
    radius:  PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    width:PropTypes.number.isRequired,
    responsive: PropTypes.bool,
    thumbnail:PropTypes.bool,
    alt: PropTypes.string,
    delay: PropTypes.number,
    quality: PropTypes.number,
    gravity: PropTypes.string,
    opacity: PropTypes.number,
    crop: PropTypes.string,
    raw:PropTypes.string,
    className :PropTypes.string
  };

  render() {
    let className = this.props.className + "";
    if (this.props.responsive){
      className += " img-responsive";
    }

    let options = {height:this.props.height, width: this.props.width, radius:this.props.radius };
    if (this.props.thumbnail) options.crop = "thumb";
    if (this.props.raw) options.raw = this.props.raw;
    if (this.props.quality) options.quality = this.props.quality;
    if (this.props.gravity) options.gravity = this.props.gravity;
    if (this.props.opacity) options.opacity = this.props.opacity;
    if (this.props.crop) options.crop = this.props.crop;

    let fullUrl = Cloudinary.createCloudinaryUrl(this.props.src, options);

    let ratio = this.props.height / this.props.width;
    let smallOptions = Object.assign(options, { height: SMALLSIZE  , width:  SMALLSIZE / ratio, quality: QUALITY });
    let smallUrl = Cloudinary.createCloudinaryUrl(this.props.src, smallOptions);

    return ( <LazyImageWrapper
             className={className}
             blurRadius={BLUR}
             alt={this.props.alt}
             width={this.props.width}
             height={this.props.height }
             delay={this.props.delay}
             src={fullUrl}
             small={smallUrl} />);
  }
}

export default ProgressiveImage;
