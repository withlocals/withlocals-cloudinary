import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'

const conditionalStyles = {
  LazyImage: {
    true: {
      opacity: 0,
    },
    false: {
      opacity: 1,
    }
  },
  FullImage: {
    true: {
      opacity: 1,
    },
    false: {
      opacity: 0,
    }
  }
};

function getStyles(target, styles, condition) {
  return Object.assign({},
    styles,
    condition && conditionalStyles[target].true,
    !condition && conditionalStyles[target].false
  )
}

class FullImage extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    let {src, onLoad, style, ...other} = this.props;
    return (
      <div style={style}>
        <img src={src} style={style} onLoad={onLoad} />
      </div>
    )
  }
}
class LazyImage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let { src, width, height, style, ...other } = this.props;
    return (
      <div style={style} >
        <svg width={this.props.width} height={this.props.height} dangerouslySetInnerHTML={{__html:`
            <filter id="blur" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
              <feGaussianBlur stdDeviation="20 20" edgeMode="duplicate" />
              <feComponentTransfer>
                <feFuncA type="discrete" tableValues="1 1" />
              </feComponentTransfer>
            </filter>
            <image filter="url(#blur)"
                   xlink:href="${src}"
                   x="0" y="0"
                   height="${height}" width="${width}"/>
                 `}}>
        </svg>
      </div>
    )
  }
}

class LazyImageWrapper extends React.Component {
  constructor(props) {
    super(props)
    this.handleLoaded = this.handleLoaded.bind(this);
    this.state = {
      loaded: false,
      addFullSizeToBody: false
    }
  }
  handleLoaded() {
    this.setState({
      loaded: true
    })
  }
  componentWillMount(){
    this.mounted = true;
    let addFullSizeToBody = ()=>{
      if (!this.mounted) return;
      this.setState({
        addFullSizeToBody: true
      });
    }
    if (this.props.delay && isFinite(this.props.delay)){
      setTimeout(addFullSizeToBody, this.props.delay);
    } else {
      addFullSizeToBody();
    }
  }
  componentWillUnmount(){
    this.mounted = false;
  }
  render() {
    let { src, small, width, height, children, className, blurRadius, delay, ...other } = this.props;
    let styles = {
      width:width,
      height:height,
      padding: 0,
      margin: 0,
      position: 'absolute',
      transform:' translateZ(0)',
      top: 0,
      transition: 'opacity 0.5s linear'
    };

    let wrapperStyles = {
      paddingBottom: styles.height,
      position: 'relative'
    };

    let fullUrl = this.state.addFullSizeToBody ? src : "";

    return (
      <div style={wrapperStyles}>
        <FullImage src={fullUrl} style={getStyles('FullImage', styles, this.state.loaded)} onLoad={this.handleLoaded} />
        <LazyImage width={width}  height={height} src={small} style={getStyles('LazyImage', styles, this.state.loaded)} blurRadius={this.props.blurRadius}/>
      </div>
    )
  }
}

export default LazyImageWrapper;
