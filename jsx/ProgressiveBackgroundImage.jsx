import React from 'react';
import LazyBackgroundImage from './LazyBackgroundImage';
import Cloudinary from './cloudinary_utils';
import PropTypes from 'prop-types'

const SMALLSIZE = 20;
const QUALITY = 30;
const BLUR = 40;

class ProgressiveBackgroundImage extends React.Component {
  static propTypes = {
    src:PropTypes.string.isRequired,
    height:PropTypes.number.isRequired,
    width:PropTypes.number.isRequired,
    thumbnail:PropTypes.bool,
    delay: PropTypes.number,
    quality: PropTypes.number,
    gravity: PropTypes.string,
    opacity: PropTypes.number,
    crop: PropTypes.string
  };

  render() {
    let {src, width, height, className, other} = this.props;

    let options = {height:this.props.height, width: this.props.width };
    if (this.props.thumbnail) options.crop = "thumb";
    if (this.props.raw) options.raw = this.props.raw;
    if (this.props.quality) options.quality = this.props.quality;
    if (this.props.gravity) options.gravity = this.props.gravity;
    if (this.props.opacity) options.opacity = this.props.opacity;
    if (this.props.crop) options.crop = this.props.crop;

    let fullUrl = Cloudinary.createCloudinaryUrl(this.props.src, options);

    let ratio = this.props.height / this.props.width;
    let smallOptions = Object.assign(options, { height: SMALLSIZE , width: SMALLSIZE / ratio, quality: QUALITY });
    let smallUrl = Cloudinary.createCloudinaryUrl(this.props.src, smallOptions);

    return (
      <LazyBackgroundImage
        className={className}
        blurRadius={BLUR}
        width={this.props.width}
        height={this.props.height }
        delay={this.props.delay}
        src={fullUrl}
        small={smallUrl}>
        {this.props.children}
      </LazyBackgroundImage>
    );
  }
}

export default ProgressiveBackgroundImage;
