import React from 'react';
import PropTypes from 'prop-types'
import Cloudinary from './cloudinary_utils';

class CloudinaryImage extends React.PureComponent {
  static propTypes = {
    src:PropTypes.string.isRequired,
    height:PropTypes.number.isRequired,
    gravity:PropTypes.string,
    crop:PropTypes.string,
    opacity:PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    raw:PropTypes.string,
    width:PropTypes.number.isRequired,
    radius:  PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    thumbnail:PropTypes.bool,
    alt: PropTypes.string
  };


  render() {
    let className = this.props.className + " img-responsive";
    let options = {height:this.props.height, width: this.props.width, radius:this.props.radius };
    if (this.props.thumbnail) options.crop = "thumb";
    if (this.props.raw) options.raw = this.props.raw;
    if (this.props.quality) options.quality = this.props.quality;
    if (this.props.gravity) options.gravity = this.props.gravity;
    if (this.props.opacity) options.opacity = this.props.opacity;
    if (this.props.crop) options.crop = this.props.crop;

    return (
      <img
        alt={this.props.alt}
        height={this.props.height}
        width={this.props.width}
        sizes={this.props.width+ "px"}
        className={className}
        src={Cloudinary.createCloudinaryUrl(this.props.src, options)}/>
    );
  }
}

export default CloudinaryImage;
