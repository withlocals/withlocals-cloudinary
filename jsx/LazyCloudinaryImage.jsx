import React from 'react';
import LazyLoad from './LazyLoad';
import CloudinaryImage from './CloudinaryImage';
import PropTypes from 'prop-types'

class LazyCloudinaryImage extends React.Component {
  static propTypes = {
    src:PropTypes.string.isRequired,
    height:PropTypes.number.isRequired,
    radius:  PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),
    width:PropTypes.number.isRequired,
  };

  render() {
    return (
      <LazyLoad {...this.props} >
        <CloudinaryImage {...this.props} />
      </LazyLoad>
    );
  }
}

export default LazyCloudinaryImage;
