import React from 'react';
import PropTypes from 'prop-types'

class LazyBackgroundImage extends React.Component {
  static propTypes = {
    src:PropTypes.string.isRequired,
    small:PropTypes.string.isRequired,
    height:PropTypes.number.isRequired,
    width:PropTypes.number.isRequired,
    delay: PropTypes.number
  };

  state = {
    src: "none", loaded:false
  };

  componentDidMount() {
    this.mounted = true;
    //this.canvas = document.createElement('canvas');
    this.loadImages();
  }

  componentDidUpdate() {
    //this.loadImages();
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  loadImages = () => {
    this.preImg = document.createElement('img');
    this.preImg.crossOrigin = 'Anonymous';
    let that = this;
    this.preImg.onload = function(){
      if (!that.mounted) return;

      let canvas = document.createElement('canvas');
      canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
      canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
      canvas.getContext('2d').drawImage(this, 0, 0);
      let dataUri = canvas.toDataURL('image/png');

      let svg = `<svg xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         width="${that.props.width}" height="${that.props.height}"
         viewBox="0 0 ${that.props.width} ${that.props.height}">
      <filter id="blur" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
        <feGaussianBlur stdDeviation="20 20" edgeMode="duplicate" />
        <feComponentTransfer>
          <feFuncA type="discrete" tableValues="1 1" />
        </feComponentTransfer>
      </filter>
      <image filter="url(#blur)"
             xlink:href="${dataUri}"
             x="0" y="0"
             height="${that.props.height}" width="${that.props.width}"/>
         </svg>`;

      if (!that.state.loaded)
        that.setState({src:"url(data:image/svg+xml;charset=utf-8,"+escape(svg)+")"});
    };

    this.preImg.src = this.props.small;

    setTimeout(()=>{
      this.realImg = document.createElement('img');
      this.realImg.crossOrigin = 'Anonymous';
      this.realImg.onload = () => {
        if (!that.mounted) return;
        that.setState({src:"url("+that.props.src+")", loaded:true});
      };

      this.realImg.src = this.props.src;
    }, this.props.delay || 0);
  };

  render() {
    let style = {
       background: this.state.src,
       backgroundSize: "cover",
       transform:' translateZ(0)',
       "-webkit-transition": "background-image 0.5s linear",
       transition: "background-image 0.5s linear",
       display: (this.state.src === "none"?"none":"block"),
       width: this.props.width,
       height: this.props.height
    };
    return (
      <div className={this.props.className} style={style}>
        {this.props.children}
      </div>
    )
  }
}

export default LazyBackgroundImage;
