import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types'

class LazyLoad extends React.Component {
  static propTypes = {
    height: PropTypes.number,
    preLoadSize: PropTypes.number
  };

  state = {
    visible: false
  };

  handleScroll = () => {

    let bounds = ReactDOM.findDOMNode(this).getBoundingClientRect();
    let scrollTop = window.pageYOffset;
    let top = bounds.top + scrollTop;
    let height = bounds.bottom - bounds.top;
    let buffer = this.props.preLoadSize || this.props.height * 2 || 100;

    let bottomOfBoxIsBelowTopOfViewPort = (top + height) > Math.max(scrollTop - buffer, 0);
    let topOfBoxIsAboveBottomOfViewPort = top  < (scrollTop + window.innerHeight + buffer);

    if (topOfBoxIsAboveBottomOfViewPort && bottomOfBoxIsBelowTopOfViewPort) {
      this.setState({visible: true});
      this.handleVisible();
    }
  };

  handleVisible = () => {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.handleScroll);
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.handleScroll);
    this.handleScroll();
  }

  componentDidUpdate() {
    if (!this.state.visible) {
      this.handleScroll();
    }
  }

  componentWillUnmount() {
    this.handleVisible();
  }

  render() {
    let preloadHeight = {
      height: this.props.height + "px"
    };
    let classes = classNames({
      'lazy-load': true,
      'lazy-load-visible': this.state.visible
    });

    return (
      <div style={preloadHeight} className={classes}>
        {this.state.visible ? this.props.children : ''}
      </div>
    );
  }
}

export default LazyLoad;
