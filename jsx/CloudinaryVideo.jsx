import React from 'react';
import PropTypes from 'prop-types'
import {Video, CloudinaryContext} from 'cloudinary-react';

export default class CloudinaryVideo extends React.PureComponent {
  static propTypes = {
    src: PropTypes.string.isRequired,
    cloudName: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired
  };

  render() {
    return (
        <CloudinaryContext cloudName={this.props.cloudName}>
          <Video
              publicId={this.props.src}
              width={this.props.width}>
          </Video>
        </CloudinaryContext>
    )
  }
}
