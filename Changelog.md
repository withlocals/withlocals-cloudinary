#CHANGELOG
see http://keepachangelog.com for more info

## [2.4.1][2019-09-06]
- support %20 characters in resource id
- support . (dot) characters in resource id

## 2.3.0 - 2018-02-26
- add cloudinary video

## 2.3.0 - 2018-02-26
- add cloudinary video

## 2.2.1 - 2017-12-12
- moved dependency to devDepedency
- improved tests

## 2.2.0 - 2017-12-09
- adding support for paths in id's

## 2.1.2 - 2017-07-12
- Round devicePixelRatio

## 2.1.1 - 2017-07-03
- Fix for the build

## 2.1.0 - 2017-07-03
- Removed dpr auto and based the dpr on the devicePixelRatio

## 2.0.0 - 2017-03-20
- upgrade all react code to es6
- clean up deps

## 1.2.6 - 2016-09-15
- found bug in regex, during playing with scala photo helper

## 1.2.5 - 2016-09-15
- removed dpr_auto because it breaks on devices with a dpr of 3 or higher
- update .gitignore

## 1.2.4 - 2016-08-29
- set dpr_auto when w_auto not set

## 1.2.3 - 2016-08-29
- fixing aspect_ratio rounding

## 1.2.2 - 2016-08-24
- fixing rounding issue

## 1.2.1 - 2016-08-24
- adding dpr auto
- removed rounding of width for backup width size

## 1.2.0 - 2016-08-24
- changing responsive url to be more cacheable by setting the backup image width to the next 100-base size

## 1.1.1 - 2016-08-24
- adding sizes attribute to cloudinary image with width set

## 1.1.0 - 2016-07-27
- moving gravity and quality from faces and 80% to 'auto' for both

## 1.0.0 - 2016-06-28
- upgrading to react 0.15
- removing immutable render mixin
- removing use of XRegExp library

## 0.7.0 - 2016-05-04
- using svg blur instead of stackblur for ProgressiveImage
- added ProgressiveBackgroundImage

## 0.6.7 - 2016-04-22
- added immutableRenderMixin

## 0.6.6 - 2016-04-19
- added support for more options in props

## 0.6.5 - 2016-04-19
- improved ProgressiveImage: 2 sizes for loading the images

## 0.6.4 - 2016-04-19
- improved ProgressiveImage: added delay property

## 0.6.3 - 2016-04-19
- improved ProgressiveImage: added animation

## 0.6.2 - 2016-04-19
- fixed ProgressiveImage

## 0.6.1 - 2016-04-18
- adding ProgressiveImage

## 0.6.0 - 2016-04-12
- moving to minimum react version 0.14
- adding raw option for more control

## 0.5.2 - 2016-02-16
- bugfix

## 0.5.1 - 2016-02-16
- added support to also parse urls like  https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_faces,q_60,f_auto/rcgqyevffk4w48z5tdw9

## 0.5.0 - 2016-02-16
- added support for urls that are not quite good, e.g.: missing protocol, missing file extension, etc.
- added 'private_cdn' option for users with a special private cdn in the format or '<account>-res.cloudinary.com'.
  When this is set, the url https://res.cloudinary.com/withlocals-com/image/upload/w_200,h_200,c_fill,g_faces,q_60,f_auto/rcgqyevffk4w48z5tdw9 will become https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_faces,q_60,f_auto/rcgqyevffk4w48z5tdw9
- added support for setting global variables

## 0.3.0 - 2015-10-07
- Added jshint and jscs
- fixed code accordingly

## 0.2.0 - 2015-10-07
- Added unit tests
- Fixed a typo in the first example of the readme

## 0.1.8 - 2015-10-07
- Changed the build to gulp-babel
- Removed webpack

## 0.1.7 - 2015-10-07
- Added babel core to the peerDependencies
- Added webpack back to the build

## 0.1.6 - 2015-10-07
- Added webpack to the devDependencies

## 0.1.5 - 2015-10-07
- Removed 'use strict' from the React files

## 0.1.4 - 2015-10-07
- Added jsx to the dependencies
- Added npmrc with a AUTH_TOKEN variable

## 0.1.3 - 2015-10-07
- Added npmrc file

## 0.1.2 - 2015-10-07
- Changed description
- Removed jest from the test script

## 0.1.1 - 2015-10-06
- Bug fix for files that were missing

## 0.1.0 - 2015-10-06
- First working version of the withlocals-cloudinary package

## 0.0.5 - 2015-10-05
- Added Babel as a webpack loader

## 0.0.4 - 2015-10-02
- Changed a let variable to var

## 0.0.3 - 2015-10-02
- Forgot to build the index.js

## 0.0.2 - 2015-10-02
- Fix for ES6

## 0.0.1 - 2015-10-02
- Setup project
