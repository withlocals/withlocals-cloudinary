var gulp = require('gulp');
var babel = require('gulp-babel');
gulp.task('watch',['default'], function () {
  return gulp.watch('jsx/**/*',['default']);
});
gulp.task('default', function () {
    return gulp.src('jsx/*.js*')
        .pipe(babel())
        .pipe(gulp.dest('scripts'));
});
