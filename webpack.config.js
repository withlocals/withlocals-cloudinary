module.exports = {
    entry: {
      CloudinaryImage: "./jsx/CloudinaryImage.jsx",
      cloudinary_utils: "./jsx/cloudinary_utils.js",
      LazyLoad: "./jsx/LazyLoad.jsx",
      LazyCloudinaryImage: "./jsx/LazyCloudinaryImage.jsx"
    },
    output: {
        path: __dirname,
        filename: "./scripts/[name].js"
    },
    module: {
      preLoaders: [
          {
             test:    /\.jsx?$/,
             exclude: /node_modules/,
             loader: 'jscs-loader'
           },
           {
               test: /\.jsx?$/, // include .js files
               exclude: /(node_modules)|(bower)/, // exclude any and all files in the node_modules folder
               loader: "jsxhint-loader"
           }
         ],
      loaders: [
        {
            test: /\.jsx?$/,
            loader: 'babel',
            query: {
               presets: ['react', 'env']
             }
        }
      ]
    },
    jshint: {
          "esnext": true,
          "camelcase": false,
          "curly":true,
          "eqeqeq": true,
          "shadow": true,
          "emitErrors": true,
          "failOnHint": true,
          "latedef":true,
          "maxdepth":3,
          "undef":true,
          "unused":true,
          "browser":true,
          "devel":true,
          "globals": {
              'fetch':false,
              "DEBUG":false
          }
      },
      jsx:{
        harmony:true
      },
      jscs: {
        "preset": "airbnb",
        "emitErrors": true,
        "failOnHint": true,
        "esnext": true,
        "validateQuoteMarks": false,
        "validateIndentation": false
      },
    jsx:{
      harmony:true
    }
};
