
import assert from 'assert';
import React from 'react';
import ReactTestUtils from 'react-dom/test-utils';
import ShallowRenderer from 'react-test-renderer/shallow';
import CloudinaryImage from '../scripts/CloudinaryImage';
import LazyLoad from '../scripts/LazyLoad';
import _ from 'lodash';

describe('Testing withlocals-cloudinary', function() {
  this.timeout(1000);

  it('createCloudinaryUrl is a function', function(done){
    const Cloudinary = require('../scripts/cloudinary_utils');
    if(typeof Cloudinary.createCloudinaryUrl === "function"){
      done();
    }
  });

  it('CloudinaryImage return an image element', function(done){

    const shallowRenderer = new ShallowRenderer();
    shallowRenderer.render(React.createElement(CloudinaryImage, { className: 'CloudinaryImage', src: "/", height: 200, width: 200 }, 'some child text'));

    const component = shallowRenderer.getRenderOutput();
    if(component.type === "img"){
      done();
    }
  });

  it('LazyLoad return a div', function(done){

    const shallowRenderer = new ShallowRenderer();
    shallowRenderer.render(React.createElement(LazyLoad, { className: 'LazyLoad', height: 200}, 'some child text'));

    const component = shallowRenderer.getRenderOutput();
    if(component.type === "div"){
      done();
    }
  });

  const idList = [
    'rcgqyevffk4w48z5tdw9',
    'family-friendly/barcelona/5',
    'family-friendly/explanation',
    'Copy%20Launches/City%20Specific/Barcelona_Gaudi_bu0xph',
    '800px-Luxembourg_Gardens__the_Medici_fountains__Paris__France__ca._1890-1900_oy6rwg'
  ];

  idList.forEach((expectedId) => {

    const givenList = [
      `https://res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}.jpg`,
      `https://res.cloudinary.com/withlocals-com/image/upload/c_fill,dpr_1.0,f_auto,fl_progressive,g_faces,h_120,q_80,r_max,w_120/${expectedId}.jpg`,
      `https://res.cloudinary.com/withlocals-com/image/upload/c_fill,dpr_1.0,f_auto,fl_progressive,g_faces,h_120,q_80,r_max,w_120/${expectedId}`,
      `//res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}.jpg`,
      `https://res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}`,
      `http://res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}.png`,
      `res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}.png`,
      `res.cloudinary.com/withlocals-com/image/upload/v1383784288/${expectedId}`,
      `res.cloudinary.com/withlocals-com/image/upload/${expectedId}`,
      `https://res.cloudinary.com/withlocals-com/image/upload/${expectedId}.jpg`,
      `https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_faces,q_60,dpr_1.0,f_auto/${expectedId}`,
      `https://withlocals-com-res.cloudinary.com/image/upload/v1383784288/${expectedId}.jpg`,
      `https://withlocals-com-res.cloudinary.com/image/upload/c_fill,dpr_1.0,f_auto,fl_progressive,g_auto,h_500,q_auto,r_0.0,w_500/${expectedId}.png`,
      `https://withlocals-com-res.cloudinary.com/image/upload/c_fill,dpr_1.0,f_auto,fl_progressive,g_auto,q_auto,w_500/${expectedId}.png`,
      `https://withlocals-com-res.cloudinary.com/image/upload/v1512740694/${expectedId}.png`,
      `https://withlocals-com-res.cloudinary.com/image/upload/v1516217710/${expectedId}.jpg`
    ];


    givenList.forEach((given) => {


      it('createCloudinaryUrl converts url : ' + given, function(done){
        global.cloudinary_config = {};
        const expected = `https://res.cloudinary.com/withlocals-com/image/upload/w_200,h_200,c_fill,g_auto,q_auto,dpr_1.0,f_auto/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:200, width:200}, true);
        assert.equal(actual, expected);
        done();
      });

      it('createCloudinaryUrl converts url, for custom subdomain : ' + given, function(done){
        const Cloudinary = require('../scripts/cloudinary_utils');
        const expected2 = `https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_auto,q_60,dpr_1.0,f_auto/${expectedId}`;
        const actual = Cloudinary.createCloudinaryUrl(given, {height:200, width:200, quality:60, private_cdn: true}, true);
        assert.equal(actual, expected2);
        done();
      });

      it('createCloudinaryUrl converts url with square picture and responsive=true, for custom subdomain : ' + given, function(done){
        const expected2B = `https://withlocals-com-res.cloudinary.com/image/upload/ar_1:1,c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:100:50/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:50, width:50, responsive:true, private_cdn: true}, true);
        assert.equal(actual, expected2B);
        done();
      });

      it('createCloudinaryUrl converts url with width defined picture and responsive=true, for custom subdomain : ' + given, function(done){
        const expected2C = `https://withlocals-com-res.cloudinary.com/image/upload/c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:100:221/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {width:221, responsive:true, private_cdn: true}, true);
        assert.equal(actual, expected2C);
        done();
      });

      it('createCloudinaryUrl converts url with width defined picture and responsive=breakpoints, for custom subdomain : ' + given, function(done){
        const expected2D = `https://withlocals-com-res.cloudinary.com/image/upload/c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:breakpoints:221/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {width:221, responsive:'breakpoints', private_cdn: true}, true);
        assert.equal(actual, expected2D);
        done();
      });

      it('createCloudinaryUrl converts url with square picture and responsive=breakpoints, for custom subdomain : ' + given, function(done){
        const expected2E = `https://withlocals-com-res.cloudinary.com/image/upload/ar_4:3,c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:breakpoints:360/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:270, width:360, responsive:'breakpoints', private_cdn: true}, true);
        assert.equal(actual, expected2E);
        done();
      });

      it('createCloudinaryUrl converts url with square picture and responsive=breakpoints, for custom subdomain : ' + given, function(done){
        const expected2E1 = `https://withlocals-com-res.cloudinary.com/image/upload/ar_1:1,c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:breakpoints:221/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:221, width:221, responsive:'breakpoints', private_cdn: true}, true);
        assert.equal(actual, expected2E1);
        done();
      });

      it('createCloudinaryUrl converts url with square picture and responsive=breakpoints, for custom subdomain : ' + given, function(done){
        const expected2E2 = `https://withlocals-com-res.cloudinary.com/image/upload/ar_232:151,c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:breakpoints:464/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:301.6, width:464, responsive:'breakpoints', private_cdn: true}, true);
        assert.equal(actual, expected2E2);
        done();
      });

      it('createCloudinaryUrl converts url with height only and responsive=true, for custom subdomain : ' + given, function(done){
        const expected2F = `https://withlocals-com-res.cloudinary.com/image/upload/h_221,c_fill,g_auto,q_auto,dpr_1.0,f_auto/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:221, responsive:true, private_cdn: true}, true);
        assert.equal(actual, expected2F);
        done();
      });


      it('createCloudinaryUrl converts url with raw params, for custom subdomain : ' + given, function(done){
        const expected3 = `https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_auto,q_60,dpr_1.0,e_colorize:30,color_blue,f_auto/${expectedId}`;
        const Cloudinary = require('../scripts/cloudinary_utils');
        const actual = Cloudinary.createCloudinaryUrl(given, {height:200, width:200, quality:60, private_cdn: true, raw:"e_colorize:30,color_blue"}, true);
        assert.equal(actual, expected3);
        done();
      });

      it('createCloudinaryUrl converts url, and respects global config from window', function(done){
        const Cloudinary = require('../scripts/cloudinary_utils');
        global.cloudinary_config = {
          quality:60, private_cdn: true
        };

        const actual = Cloudinary.createCloudinaryUrl(given, {height:200, width:200}, true);
        const expected = `https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_auto,q_60,dpr_1.0,f_auto/${expectedId}`;
        assert.equal(actual, expected);
        done();
      });
      it('createCloudinaryUrl converts url, and respects global config from window #2', function(done){
        const Cloudinary = require('../scripts/cloudinary_utils');
        global.cloudinary_config = {
          quality:60
        };
        const actual = Cloudinary.createCloudinaryUrl(given, {height:200, width:200}, true);
        const expected = `https://res.cloudinary.com/withlocals-com/image/upload/w_200,h_200,c_fill,g_auto,q_60,dpr_1.0,f_auto/${expectedId}`;
        assert.equal(actual, expected);
        done();
      });
      it('createCloudinaryUrl converts url, and respects global config from window #3', function(done){
        const Cloudinary = require('../scripts/cloudinary_utils');
        global.cloudinary_config = {
          responsive:"breakpoints"
        };
        const actual = Cloudinary.createCloudinaryUrl(given, {height:338, width:221}, true);
        const expected = `https://res.cloudinary.com/withlocals-com/image/upload/ar_17:26,c_fill,g_auto,q_auto,dpr_1.0,f_auto/w_auto:breakpoints:221/${expectedId}`;
        assert.equal(actual, expected);

        done();
      });
    });

  });

})
