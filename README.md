# withlocals-cloudinary

 [![NPM version][npm-image]][npm-url]

[ ![Codeship Status for withlocals/Withlocals-Cloudinary](https://codeship.com/projects/ea84ec70-4e54-0133-eaf7-524cf6105349/status?branch=master)](https://codeship.com/projects/106843)

## Usage
Install via npm: `npm install withlocals-cloudinary --save`

### Create a Cloudinary url
This is an example how `Cloudinary.createCloudinaryUrl` can be used:
```javascript
var Cloudinary = require('withlocals-cloudinary').Cloudinary;
//...
render: function() {
  return (<img href={Cloudinary.createCloudinaryUrl(url, options)} />);
}
//...
```


This is an example how `options` can look like:
```javascript
var options = {
  width: 200, // int - required
  height: 200, //int - required
  quality: 80, // int
  screenwidth: true, // boolean
  screenheight: true, // boolean
  gravity: 'faces', // string
  gravity2: 'center', // string
  crop: 'fill', // string
  flags: 'progressive', // string
  protocol: 'https://', // string
  radius: "20", // string
  border: "4px_solid_black", // string
  x: 200, // int
  y: 200, // int
  opacity: 80, // int
  background: "rgb:9090ff", // string
  e: 200, // int
  brightness: 80, // int
  saturation: 80, // int
  fetch_format: "auto" // string
}
```
`options` should at least have a `width` and `height`.
For more info about the image transformations you can take a look at the [Cloudinary documentation](http://cloudinary.com/documentation/image_transformations#reference)

#### Exta options, and global variables
For our personal use we added some extra options,

- private_cdn : this changes your urls from  https://res.cloudinary.com/withlocals-com/image/upload/w_200,h_200,c_fill,g_faces,q_60,f_auto/rcgqyevffk4w48z5tdw9 to https://withlocals-com-res.cloudinary.com/image/upload/w_200,h_200,c_fill,g_faces,q_60,f_auto/rcgqyevffk4w48z5tdw9. Which is used for advanced accounts with a private cdn which support ( today 2016 Feb.) HTTP/2
- cdn_load_balance : for users that want to use subdomain load balancing, i.e. people without http2

All options that are stored in e.g. ```global.cloudinary_config = {quality:60}``` or ```window.cloudinary_config = {quality:60}```, depending on your environment, will be taken into account. However they will be overriden by local settings.

### React component with a Cloudinary image
This is an example how `<CloudinaryImage />` can be used:
```javascript
var CloudinaryImage = require('withlocals-cloudinary').CloudinaryImage;

//...
render: function() {
  return (<CloudinaryImage
              src={url}
              height={height}
              width={width}
              thumbnail={thumbnail}
              alt={alt} />);
}
//...
```
`src`, `width` and `height` are required. `thumbnail` and `alt` are optional.

### Lazy load content
This is an example how `<LazyLoad />` can be used:
```javascript
var LazyLoad = require('withlocals-cloudinary').LazyLoad;

//...
render: function() {
  return (<LazyLoad
              height={height}
              preLoadSize={preLoadSize}>
              //...
          </LazyLoad>);
}  
//...
```
### Lazy load a Cloudinary Image
This is an example how `<LazyCloudinaryImage />` can be used:
```javascript
var LazyCloudinaryImage = require('withlocals-cloudinary').LazyCloudinaryImage;

//...
render: function() {
  return (<LazyCloudinaryImage
              src={url}
              height={height}
              width={width} />);
}  
//...
```

## Development
the webpack command will run jshint and jscs on your code,
but gulp will actually build it.

Pull requests are welcome!

Want to publish a new version? Follow these steps:

1. `npm install` - install local dependencies
2. change version number in the `package.json`
3. `npm publish` - publish the code to npm


[npm-image]: https://badge.fury.io/js/withlocals-cloudinary.svg
[npm-url]: https://npmjs.org/package/withlocals-cloudinary
