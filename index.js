var Cloudinary = require('./scripts/cloudinary_utils');
var CloudinaryImage = require('./scripts/CloudinaryImage');
var LazyLoad = require('./scripts/LazyLoad');
var LazyCloudinaryImage = require('./scripts/LazyCloudinaryImage');
var ProgressiveImage = require('./scripts/ProgressiveImage');
var CloudinaryVideo = require('./scripts/CloudinaryVideo');

module.exports = {
  Cloudinary: Cloudinary,
  CloudinaryImage: CloudinaryImage,
  LazyLoad: LazyLoad,
  LazyCloudinaryImage: LazyCloudinaryImage,
  ProgressiveImage: ProgressiveImage,
  CloudinaryVideo: CloudinaryVideo
};
