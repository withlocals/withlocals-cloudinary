module.exports = {
  "parser": "babel-eslint",
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:flowtype/recommended"
    ],
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "globals": {
      'fetch': false,
      "DEBUG": false,
      "URL_AGENCY_HOST": false,
      "URL_API_HOST": false,
      "L": false,
      "StripeCheckout": false,
      "FB": false
  },

   "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  settings: {
    'import/extensions': ['.jsx','.js']
  },

  "plugins": ["react","jsx-a11y","import", "flowtype"],

  "rules": {
    "space-after-keywords" : 0,
    "space-return-throw-case": 0,
    "no-extra-boolean-cast": 0, // i like to double negate strings to booleans
    "no-empty-label": 0,
    // Disable `no-console` rule
    "no-console": 0,
    //keyword-spacing: [1, { "before": true }],
    "no-labels": "error",
    // Give a warning if identifiers contain underscores

    "quotes": [0, "double"],
    "comma-dangle": 1,
    "no-mixed-spaces-and-tabs": 1,
    "no-empty": 2,
    "no-undef": 2,
    "no-unused-vars": 2,
    "no-underscore-dangle": 2,
    // TODO: fix code, then change to levels +1
    "prefer-template": 0,
    "prefer-const": 1,
    "react/prefer-stateless-function": 1,
    "object-shorthand": 0,
    "react/jsx-boolean-value": 1,
    "keyword-spacing": 0,
    "flowtype/space-before-type-colon":0,
    "flowtype/space-after-type-colon":0,
  }
}
